/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package de.etern.it.f3hh.implement;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 * Klasse zum Updaten des Programms.
 *
 * @author Dennis van der Wals
 *
 */
public class Updater {
  private final Version version;
  private final String source;
  private final String name;
  private JProgressBar download;

  public Updater() {
    this.version = Meta.v;
    this.source = Meta.SERVER + Meta.SUB_ADD + "/dateien/";
    this.name = Meta.APP_NAME + ".jar";
  }

  /**
   * Standardkonstruktor, der sich alle Informationen aus der server-Klasse holt.
   */
  public Updater(Version version) {
    this.version = version;
    this.source = Meta.SERVER + Meta.SUB_ADD + "/dateien/";
    this.name = Meta.APP_NAME + ".jar";
  }

  /**
   * Standardkonstruktor.
   * 
   * @param version Softwareversion
   * @param source Internetaddresse bsp: "http://dionysios.di.ohost.de/test/" oder Unterverzeichnis
   *        des Standardadresse: "test/"
   * @param name Dateiname bsp: "Reaktor" ohne ".jar"
   */
  public Updater(Version version, String source, String name) {
    this.version = version;
    if (source.contains("http://")) {
      this.source = source;
    } else {
      this.source = Meta.SERVER + source;
    }
    this.name = name + ".jar";
  }

  /**
   * Laedt eine Datei von der angegebenen URL auf die angegebene Datei.
   * 
   * @param url Quelldatei
   * @param os Zieldatei
   * @throws IllegalStateException
   * @throws java.net.MalformedURLException
   * @throws java.net.ProtocolException
   * @throws java.io.IOException
   */
  void downloadFile(URL url, OutputStream os) throws IllegalStateException, IOException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("GET");
    conn.connect();
    int responseCode = conn.getResponseCode();
    if (responseCode == HttpURLConnection.HTTP_OK) {
      byte tmp_buffer[] = new byte[4096];
      InputStream is = conn.getInputStream();
      int n;
      while ((n = is.read(tmp_buffer)) > 0) {
        os.write(tmp_buffer, 0, n);
        os.flush();
        this.download.setValue(this.download.getValue() + n);
      }
    } else
      throw new IllegalStateException("HTTP response: " + responseCode); //$NON-NLS-1$
  }

  /**
   * Sucht nach Updates auf dem Server und laedt diese herunter.
   * 
   * @throws java.io.IOException
   */
  public void getUpdate() throws IOException {
    URL url = new URL(this.source + Meta.VERSIONSDATEI);
    Scanner scan = new Scanner(url.openStream());
    // double v = Double.parseDouble(scan.next());
    try {
      Version v = new Version(scan.nextLine());

      // Update laden
      // if (this.version < v) {
      if (this.version.compareTo(v) == -1) {

        // Updatefrage
        String[] options = {"Ja", "Nein"};
        int state = JOptionPane.showOptionDialog(null,
            "<html>Eine neue Version steht zur Verf\u00FCgung.<br>Soll sie heruntergeladen werden?</html>",
            "Upgrade", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
            options[0]);

        if (state == 0) {

          // Fortschrittsbalken erzeugen
          this.download = new JProgressBar(0,
              new URL(this.source + this.name).openConnection().getContentLength());
          this.download.setStringPainted(true);

          // Download starten
          new Thread(() -> {
            try {
              Updater.this.downloadFile(new URL(Updater.this.source + Updater.this.name),
                  new FileOutputStream(Updater.this.name));
            } catch (Exception ignored) {
            }
          }).start();

          // Fortschritt anzeigen
          String[] option = {"Ok"};
          while (this.download.getValue() != this.download.getMaximum()) {
            JOptionPane.showOptionDialog(null, this.readVersionHistory(scan), "Upgrade",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, option, option[0]);
          }

          // Neustart der Anwendung
          if (!System.getProperty("os name").contains("linux")) {
            Runtime.getRuntime().exec("javaw -jar " + new File(this.name).getAbsolutePath());
            System.out.println("nich linux");
          } else {
            Runtime.getRuntime().exec("java -jar " + new File(this.name).getAbsolutePath());
            System.out.println("linux");
          }

          // Programm beenden
          System.exit(0);
        }
      }
    } catch (Exception e) {
      // TODO: handle exception
    } finally {
      scan.close();
    }
  }

  /**
   * Liest die letzte Versionshistory aus.
   * 
   * @return JPanel fuer Updatefenster
   * @throws IllegalStateException
   * @throws java.io.IOException
   */
  private JPanel readVersionHistory(Scanner scan) throws IllegalStateException {

    // Die Versionshistory wird gelesen
    String s = "";
    scan.nextLine();
    while (scan.hasNextLine()) {
      s += scan.nextLine() + "\n";
    }

    // Ein scrollbares Textfeld wird damit gef�llt
    JTextArea history = new JTextArea();
    history.setLineWrap(true);
    history.setWrapStyleWord(true);
    history.setText(s);

    JScrollPane scrollPane = new JScrollPane(history);
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

    // JLabel info = new JLabel(
    // "<html>Ein Update wurde geladen.<br>Bitte starten Sie das Programm
    // neu. </html>");

    JLabel info = new JLabel(
        "<html>Ein Update wird geladen.<br>Das Programm startet im Anschluss neu. </html>");

    // Komponenten in Container packen
    JPanel container = new JPanel();
    container.add(info);
    container.add(scrollPane);
    container.add(this.download);

    // Layout anlegen
    GridBagLayout gbl = new GridBagLayout();
    this.setGridBagConstraints(gbl, this.download, 0, 0.10, GridBagConstraints.HORIZONTAL);
    this.setGridBagConstraints(gbl, info, 1, 0.10, GridBagConstraints.HORIZONTAL);
    this.setGridBagConstraints(gbl, scrollPane, 2, 0.80, GridBagConstraints.BOTH);
    container.setLayout(gbl);
    container.setPreferredSize(new Dimension(300, 200));

    return container;
  }

  /**
   * Standardfunktion fuer das GridbagLayout.
   * 
   * @param gbl Gridbaglayout
   * @param c Compnent
   * @param gridy Zeile
   * @param weighty Zeilengewicht
   * @param fill Verbreiterung
   */
  private void setGridBagConstraints(GridBagLayout gbl, Component c, int gridy, double weighty,
      int fill) {
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridy = gridy;
    gbc.gridwidth = 1;
    gbc.weightx = 1.0;
    gbc.weighty = weighty;
    gbc.fill = fill;
    gbl.setConstraints(c, gbc);
  }

}
