/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/

package de.etern.it.f3hh.core;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * Analyse der Passwortvorschl�ge.
 *
 * @author Dennis van der Wals
 *
 */
public class Analyzer {
	private LinkedList<String> words;
	private String[] tried;
	private int[] hits;
	private int index;
	private int wortLaenge;
	private final int maxLvl;
	private boolean hacked;

	/**
	 * Erzeugt einen neuen Analyzer, welcher nach maxLvl die Loesung liefern
	 * muss.
	 * 
	 * @param maxLvl
	 *            maximale Anzahl der Versuche
	 */
	public Analyzer(int maxLvl) {
		this.hacked = false;
		this.maxLvl = maxLvl;
	}

	// public boolean change(String alt, String neu) {
	// words.remove(alt);
	// words.add(neu);
	// LinkedList<String> check = new LinkedList<String>();
	// check.addAll(words);
	// int start = 0;
	// for (int i = 0; i <= index; i++) {
	// if (tried[i].equals(alt))
	// words = getFollower(neu, words, treffer)
	// }
	// return true;
	// }

	/**
	 * Gibt die Anzahl der uebereinstimmenden Buchstaben zurueck.
	 * 
	 * @param alt
	 *            alter String
	 * @param neu
	 *            neuer String
	 * @return anzahl der identischen Buchstaben
	 */
	private int compare(String alt, String neu) {
		int zaehler = 0;
		for (int i = 0; i < alt.length(); i++)
			if (alt.substring(i, i + 1).equals(neu.substring(i, i + 1))) {
				zaehler++;
			}
		return zaehler;
	}

	/**
	 * Zaehlt ueber 4 Ebenen, wie oft ein Wort in jeder moeglichen Kombination
	 * an richtig platzierten Buchstaben, innerhalb von 4 Ebenen aufgesp�hrt
	 * wird.
	 * 
	 * @param woerter
	 *            Woerterliste
	 * @param level
	 *            Ebene
	 * @param zaehler
	 *            Zaehler
	 */
	private void counter(LinkedList<String> woerter, int level, HashMap<String, Integer> zaehler) {
		LinkedList<String> follow;
		for (String wort : woerter) {
			// zaehler.put(wort, zaehler.get(wort) + 1);
			for (int i = 0; i < (this.wortLaenge - 1); i++) {
				follow = this.getFollower(wort, woerter, i);
				// for (String string : follow) {
				// zaehler.put(string, zaehler.get(string) + 1);
				// }
				if (level != this.maxLvl) {
					this.counter(follow, level + 1, zaehler);
				} else if (follow.size() == 1) {
					for (String string : follow) {
						zaehler.put(string, zaehler.get(string) + 1);
					}
				}
			}
		}
	}

	/**
	 * Erstellt eine Liste von Woertern aus der gegebenen Wortmenge, welche eine
	 * Anzahl von Buchstaben mit dem Quellwort gemeinsam haben.
	 * 
	 * @param quelle
	 *            Quellwort
	 * @param woerter
	 *            Woerterliste
	 * @param treffer
	 *            Anzahl identischer Buchstaben
	 * @return Liste mit passenden Woertern
	 */
	private LinkedList<String> getFollower(String quelle, LinkedList<String> woerter, int treffer) {
		LinkedList<String> neu = new LinkedList<>();
		for (String string : woerter)
			if (this.compare(quelle, string) == treffer) {
				neu.add(string);
			}
		return neu;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return this.index;
	}

	/**
	 * Findet aus einer Hashmap das Wort aus einer Wortliste mit dem groe�tem
	 * Integerwert.
	 * 
	 * @param zaehler
	 *            Zaehler
	 * @param woerter
	 *            Woerterliste
	 * @return Wort mit groe�tem Integerwert
	 */
	private String getMax(HashMap<String, Integer> zaehler, LinkedList<String> woerter) {
		String max = null;
		int m = -1;
		for (String string : woerter) {
			int n = zaehler.get(string);
			if (n > m) {
				m = n;
				max = string;
			}
		}
		return max;
	}

	/**
	 * Gibt das naechste Wort, welches zu testen ist, wieder aus.
	 * 
	 * @return naechstes Wort
	 */
	public String getNext(int treffer) {
		if (this.index == this.maxLvl)
			return null;
		this.hits[this.index] = treffer;
		this.words = this.getFollower(this.tried[this.index], this.words, this.hits[this.index]);
		this.index++;
		this.sort();
		this.tried[this.index] = this.words.getFirst();
		if (this.words.size() == 1) {
			this.hacked = true;
		}
		return this.tried[this.index];
	}

	/**
	 * @return the wortLaenge
	 */
	public int getWortLaenge() {
		return this.wortLaenge;
	}

	/**
	 * @return the hasHacked
	 */
	public boolean isHacked() {
		return this.hacked;
	}

	// /**
	// * Findet aus einer Hashmap das Wort aus einer Wortliste mit dem kleinstem
	// * Integerwert.
	// *
	// * @param zaehler
	// * Zaehler
	// * @param woerter
	// * Woerterliste
	// * @return Wort mit groe�tem Integerwert
	// */
	// private String getMin(HashMap<String, Integer> zaehler,
	// LinkedList<String> woerter) {
	// String min = null;
	// int m = Integer.MAX_VALUE;
	// for (String string : woerter) {
	// int n = zaehler.get(string);
	// if (n <= m) {
	// m = n;
	// min = string;
	// }
	// }
	// return min;
	// }

	/**
	 * Fuegt eine Liste von Woertern hinzu, wenn alle die gleiche Laenge haben.
	 * 
	 * @param input
	 *            Wortliste
	 * @return Wahrheitswert
	 */
	boolean setWords(LinkedList<String> input) {
		this.words = new LinkedList<>();
		for (String string : input) {
			this.words.add(string);
			this.wortLaenge += string.length();
		}
		this.wortLaenge = (int) Math.round(((double) this.wortLaenge) / this.words.size());
		for (String word : this.words)
			if (word.length() != this.wortLaenge) {
				JOptionPane.showMessageDialog(null, "Das Wort: " + word + " hat eine abweichende L�nge");
				return false;
			}
		return true;
	}

	/**
	 * Liest Woerter aus einer Eingabe, ueberprueft sie und fuegt sie dem
	 * Programm hinzu.
	 * 
	 * @param input
	 *            Wortliste
	 * @return Wahrheitswert
	 */
	public boolean setWords(String input) {
		Scanner scan = new Scanner(input);
		scan.useDelimiter("\n");
		LinkedList<String> liste = new LinkedList<>();
		while (scan.hasNext()) {
			liste.add(scan.next());
		}

		scan.close();
		return this.setWords(liste);
	}

	/**
	 * Die Methode sortiert die Woerterliste so, dass das wahrscheinlichste Wort
	 * als erstes abgerufen wird.
	 */
	private void sort() {
		HashMap<String, Integer> zaehler = new HashMap<>();
		for (String wort : this.words) {
			zaehler.put(wort, 0);
		}
		this.counter(this.words, this.index + 1, zaehler);
		LinkedList<String> newWords = new LinkedList<>();
		while (zaehler.size() > 0) {
			String next = this.getMax(zaehler, this.words);
			zaehler.remove(next);
			this.words.remove(next);
			newWords.add(next);
		}
		this.words = newWords;
	}

	/**
	 * Gibt das erste Wort zurueck.
	 * 
	 * @return gibt das erste Wort zurueck.
	 */
	public String start() {
		this.tried = new String[this.maxLvl];
		this.hits = new int[this.maxLvl];
		this.index = 0;
		this.sort();
		this.tried[this.index] = this.words.getFirst();
		return this.tried[this.index];
	}
}
