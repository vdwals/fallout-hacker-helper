/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/

package de.etern.it.f3hh.gui;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import de.etern.it.f3hh.core.Analyzer;
import de.etern.it.f3hh.implement.Updater;

/**
 * Analyse der Passwortvorschläge.
 *
 * @author Dennis van der Wals
 *
 */
public class F3Hack {
	private final static int MAXLVL = 4;

	/**
	 * Mainmethode.
	 *
	 * @param args
	 *            keine Funktion
	 * @throws java.io.IOException
	 */
	public static void main(String[] args) throws IOException {
		new F3Hack();
	}

	private Analyzer brain;
	// Textfelder
	private JTextField treffer;
	private JTextArea pwField;
	private JScrollPane klarText;
	// Fenster
	private JFrame frame;
	private ActionListener action;
	private JButton ok;
	private JButton starten;
	private JButton reset;
	// Label
	private JLabel first;
	private JLabel second;
	private JLabel third;
	private JLabel firstHits;
	private JLabel secondHits;
	private JLabel thirdHits;

	private JLabel pwListe;

	private int y;

	/**
	 * Erzeugt das Fenster.
	 */
	private F3Hack() {
		Updater update = new Updater();
		try {
			update.getUpdate();
		} catch (IOException ignored) {
		}
		this.y = 30;
		this.frame = this.getFrame();
		this.frame.add(this.getPwListe());
		this.frame.add(this.getPwText());
		this.frame.add(this.getStarten());
		this.frame.add(this.getReset());
		this.frame.add(this.getFirstHits());
		this.frame.add(this.getSecondHits());
		this.frame.add(this.getThirdHits());
		this.frame.setSize(500, 400);
		this.frame.setResizable(false);
		this.frame.setVisible(true);
	}

	/**
	 * Gibt Actionlistener zurueck oder initialisiert es.
	 *
	 * @return action
	 */
	ActionListener getAction() {
		if (this.action == null) {
			this.action = arg0 -> {
				if (arg0.getSource() == F3Hack.this.starten) {
					F3Hack.this.brain = new Analyzer(MAXLVL);
					if (F3Hack.this.brain.setWords(F3Hack.this.pwField.getText().toUpperCase())) {
						F3Hack.this.frame.add(F3Hack.this.getTreffer());
						F3Hack.this.frame.add(F3Hack.this.getOk());
						F3Hack.this.frame.add(F3Hack.this.getFirst());
						F3Hack.this.first.setText(F3Hack.this.brain.start());
						F3Hack.this.frame.add(F3Hack.this.getSecond());
						F3Hack.this.frame.add(F3Hack.this.getThird());
					}
				} else if (arg0.getSource() == F3Hack.this.ok) {
					int hits = Integer.parseInt(F3Hack.this.treffer.getText());
					String next = F3Hack.this.brain.getNext(hits);
					String info = hits + "/" + F3Hack.this.brain.getWortLaenge();
					if (F3Hack.this.brain.isHacked()) {
						JOptionPane.showMessageDialog(null, "Passeord: " + next);
						F3Hack.this.reset();
					} else {
						switch (F3Hack.this.brain.getIndex()) {
						case 1:
							F3Hack.this.second.setText(next);
							F3Hack.this.firstHits.setText(info);
							break;
						case 2:
							F3Hack.this.third.setText(next);
							F3Hack.this.secondHits.setText(info);
							break;
						case 3:
							F3Hack.this.thirdHits.setText(info);
							if (!F3Hack.this.brain.isHacked()) {
								JOptionPane.showMessageDialog(null, "An error occured. Please restart hacking.");
								F3Hack.this.reset();
								F3Hack.this.y -= 25;
							}
							break;
						default:
							break;
						}
						F3Hack.this.treffer.setText("");
						F3Hack.this.y += 25;
						F3Hack.this.ok.setBounds(400, F3Hack.this.y, 70, 20);
						F3Hack.this.treffer.setBounds(350, F3Hack.this.y, 30, 20);
					}
				} else if (arg0.getSource() == F3Hack.this.reset) {
					F3Hack.this.reset();
				}
				F3Hack.this.frame.repaint();
			};
		}
		return this.action;
	}

	JLabel getFirst() {
		if (this.first == null) {
			this.first = new JLabel("");
			this.first.setBounds(250, 30, 290, 20);
		}
		return this.first;
	}

	JLabel getFirstHits() {
		if (this.firstHits == null) {
			this.firstHits = new JLabel("");
			this.firstHits.setBounds(350, 30, 290, 20);
		}
		return this.firstHits;
	}

	/**
	 * Gibt frame zurueck oder initialisiert es.
	 *
	 * @return frame
	 */
	JFrame getFrame() {
		if (this.frame == null) {
			this.frame = new JFrame("Decoder");
			this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			this.frame.setLayout(null);
		}
		return this.frame;
	}

	/**
	 * Gibt den encrpt Button zurueck oder initialisiert ihn.
	 *
	 * @return encrpt Button
	 */
	JButton getOk() {
		if (this.ok == null) {
			this.ok = new JButton("Ok");
			this.ok.addActionListener(this.getAction());
			this.ok.setBounds(400, this.y, 70, 20);
		}
		return this.ok;
	}

	/**
	 * Gibt das Klartextfeld zurueck oder initialisiert es.
	 *
	 * @return Klartextfeld
	 */
	JTextArea getPwField() {
		if (this.pwField == null) {
			this.pwField = new JTextArea();
			this.pwField.setLineWrap(true);
			this.pwField.setWrapStyleWord(true);
			this.pwField.setFont(
					new Font(Font.MONOSPACED, this.pwField.getFont().getStyle(), this.pwField.getFont().getSize()));
		}
		return this.pwField;
	}

	JLabel getPwListe() {
		if (this.pwListe == null) {
			this.pwListe = new JLabel("Enter passwords:");
			this.pwListe.setBounds(5, 5, 200, 20);
		}
		return this.pwListe;
	}

	/**
	 * Gibt Klartextfeld mit Scrollbar zurueck oder initialisiert es.
	 *
	 * @return Klartextfeld mit Scrollbar
	 */
	JScrollPane getPwText() {
		if (this.klarText == null) {
			this.klarText = new JScrollPane(this.getPwField());
			this.klarText.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			this.klarText.setBounds(5, 30, 220, 305);
		}
		return this.klarText;
	}

	/**
	 * @return the reset
	 */
	JButton getReset() {
		if (this.reset == null) {
			this.reset = new JButton("Reset");
			this.reset.addActionListener(this.getAction());
			this.reset.setBounds(250, 340, 220, 20);
		}
		return this.reset;
	}

	JLabel getSecond() {
		if (this.second == null) {
			this.second = new JLabel("");
			this.second.setBounds(250, 55, 290, 20);
		}
		return this.second;
	}

	JLabel getSecondHits() {
		if (this.secondHits == null) {
			this.secondHits = new JLabel("");
			this.secondHits.setBounds(350, 55, 290, 20);
		}
		return this.secondHits;
	}

	/**
	 * Gibt den password Button zurueck oder initialisiert ihn.
	 *
	 * @return password Button
	 */
	JButton getStarten() {
		if (this.starten == null) {
			this.starten = new JButton("Start");
			this.starten.addActionListener(this.getAction());
			this.starten.setBounds(5, 340, 220, 20);
		}
		return this.starten;
	}

	JLabel getThird() {
		if (this.third == null) {
			this.third = new JLabel("");
			this.third.setBounds(250, 80, 290, 20);
		}
		return this.third;
	}

	JLabel getThirdHits() {
		if (this.thirdHits == null) {
			this.thirdHits = new JLabel("");
			this.thirdHits.setBounds(350, 80, 290, 20);
		}
		return this.thirdHits;
	}

	JTextField getTreffer() {
		if (this.treffer == null) {
			this.treffer = new JTextField();
			this.treffer.setBounds(350, this.y, 30, 20);
		}
		return this.treffer;
	}

	/**
	 * Setzt die grafische Oberflaeche zurueck.
	 */
	private void reset() {
		this.first.setText("");
		this.second.setText("");
		this.third.setText("");
		this.firstHits.setText("");
		this.secondHits.setText("");
		this.thirdHits.setText("");
		this.pwField.setText("");
		this.treffer.setText("");
		this.y = 30;
		this.ok.setBounds(400, this.y, 70, 20);
		this.treffer.setBounds(350, this.y, 30, 20);
		this.frame.remove(this.treffer);
		this.frame.remove(this.ok);
	}
}
